// ICON IMPORTS
import {
    faCircle,
    faArrowLeft,
    faArrowRight,
    faChevronLeft,
    faChevronRight,
} from '@fortawesome/free-solid-svg-icons';

const Icons = {
    faArrowLeft,
    faArrowRight,
    faCircle,
    faChevronLeft,
    faChevronRight,
};

// EXPORTS
export default Icons;
