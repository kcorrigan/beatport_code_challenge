export const images = [
    'https://lp-cms-production.imgix.net/2019-06/1da32e09c1497a907cda4479049e6b2b-denver.jpg?fit=crop&q=40&sharp=10&vib=20&auto=format&ixlib=react-8.6.4',
    'https://images.unsplash.com/photo-1535406208535-1429839cfd13?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    'https://media.istockphoto.com/photos/classical-guitar-on-a-dark-background-picture-id484766226?k=6&m=484766226&s=612x612&w=0&h=l8qCot6k_pizuZS_mAEJIT87RbQoqsB6AtGku3QqnwI=',
    'https://www.sciencenewsforstudents.org/wp-content/uploads/2020/03/1030_onwardunicorns-1-1028x579.png',
];

export const verticalImages = [
    [
        'https://www.universetoday.com/wp-content/uploads/2020/01/EarthCap.jpg',
        'https://images.unsplash.com/photo-1535406208535-1429839cfd13?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    ],
    [
        'https://media.istockphoto.com/photos/classical-guitar-on-a-dark-background-picture-id484766226?k=6&m=484766226&s=612x612&w=0&h=l8qCot6k_pizuZS_mAEJIT87RbQoqsB6AtGku3QqnwI=',
        'https://www.nasa.gov/sites/default/files/bwhi1apicaaamlo.jpg_large.jpg',
        'https://www.quickanddirtytips.com/sites/default/files/styles/article_main_image/public/images/19571/astronaut-in-space-compressor.png?itok=3xPZVjBk',
    ]
];
