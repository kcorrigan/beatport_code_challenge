// PACKAGES
import { css } from '@emotion/core';
import React from 'react';
import ReactDOM from 'react-dom';
// CONSTANTS
import { images, verticalImages } from './Constants/images';
// COMPONENTS
import Slider from './Slider';
// UTILS
import { initializeIcons } from './Utils/icon-utils';

// Add icons at app startup
initializeIcons();

ReactDOM.render(
    <div css={css`display: flex`}>
        <Slider
            slides={images}
            scrollInterval={5}
            width={40}
        />
        <Slider
            slides={verticalImages}
            scrollInterval={0}
            width={35}
            height={70}
            controlsOnHover
            nextControlIcon="chevron-right"
            prevControlIcon="chevron-left"
            useGrid
        />
    </div>,
    document.getElementById('root')
);
