// PACKAGES
import { library } from '@fortawesome/fontawesome-svg-core';
// CONSTANTS
import Icons from '../Constants/icons';

export const initializeIcons = () => {
    library.add({ ...Icons });
};
