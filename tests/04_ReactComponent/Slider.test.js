// PACKAGES
import { mount } from 'enzyme';
import React from 'react';
import Slider from './Slider';
// LIBS
import { images } from './Libs/images';

describe('Slider', () => {
    it('mounts', () => {
        const container = mount(
            <Slider 
                slides={images}
            />
        );
        expect(container.find('.slider').exists()).toBe(true);
    });
});
