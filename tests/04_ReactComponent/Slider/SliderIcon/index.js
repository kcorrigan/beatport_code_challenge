// PACKAGES
import { css } from '@emotion/core';
import React from 'react';
// CONSTANTS
import { fadeIn } from '../../Constants/animations';

const SliderIcon = ({ direction, icon, onClick }) => {
    const translateX = direction === 'left' ? '-.25rem' : '.25rem';
    const horizontalPosition = direction === 'right' ? 'right: 1.5rem' : 'left: 1.5rem';

    return (
        <div
            role="button"
            tabIndex="0"
            onClick={onClick}
            css={SliderIconCSS({
                horizontalPosition,
                translateX,
            })}
        >
            {icon}
        </div>
    );
};

// STYLES
const SliderIconCSS = ({ horizontalPosition, translateX }) => (
    css`display: flex;
    position: absolute;
    justify-content: center;
    align-items: center;
    top: 45%;
    ${horizontalPosition};
    height: 3.5rem;
    width: 3.5rem;
    background: whitesmoke;
    border-radius: 50%;
    cursor: pointer;
    transition: transform ease-in 0.1s;
    animation: ${fadeIn} .2s ease-in;
    outline: none;

        &:hover {
            transform: scale(1.1);
        }

        img {
            transform: translateX(${translateX});
        }
    }`
);

// EXPORTS
export default SliderIcon;
