// PACKAGES
import { css } from '@emotion/core';
import React from 'react';
// CONSTANTS
import { fadeIn } from '../../Constants/animations';

const Slide = ({ imageUrl }) => (
    <div
        key={imageUrl}
        css={SlideCss({ imageUrl })}
    />
);

// STYLES
const SlideCss = ({ imageUrl }) => (
    css`height: 100%;
    width: 100%;
    background-image: url('${imageUrl}');
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    margin: .25rem;
    border-radius: 1rem;
    animation: ${fadeIn} .5s ease-in;`
);

// EXPORTS
export default Slide;
