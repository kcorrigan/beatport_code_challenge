// PACKAGES
import { css } from '@emotion/core';

const SliderContent = ({ children, isGrid }) => {
    return (
        <div css={SliderContentCSS({ isGrid })}>
            {children}
        </div>
    );
};

// STYLES
const SliderContentCSS = ({ isGrid }) => (
    css`height: 100%;
    width: 100%;
    display: flex;
    ${isGrid && 'flex-direction: column'}`
);

// EXPORTS
export default SliderContent;
