// PACKAGES
import { css } from '@emotion/core';
import React from 'react';

const SliderPagination = ({ currentSlideIdx, icon, slides }) => {
    return (
        <div
            css={SliderPaginationCSS}
        >
            {slides.map((slide, idx) => (
                <span
                    key={`${slide + idx}`}
                    css={SliderPaginationIconCSS({ isActive: currentSlideIdx === idx })}
                >
                    {icon}
                </span>
            ))}
        </div>
    );
};

// STYLES
const SliderPaginationCSS = (
    css`display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 1.5rem;
    width: 100%;`
);

const SliderPaginationIconCSS = ({ isActive }) => (
    css`color: ${isActive ? 'tomato' : 'white'};
    margin: 0 .25rem;`
);

// EXPORTS
export default SliderPagination;
