// PACKAGES
import { css } from '@emotion/core';
import PropTypes from 'prop-types';
import React, { useEffect, useRef, useState } from 'react';
// CUSTOM HOOKS
import { useCurrentWidth } from '../CustomHooks';
// COMPONENTS
import Icon from '../Icon';
import Slide from './Slide';
import SliderContent from './SliderContent';
import SliderIcon from './SliderIcon';
import SliderPagination from './SliderPagination';

/**
 * @type {React.Component}
 *
 * @description Create a Slider/Carousel using modern react. It's up to you to add styles.
 * Sass is available, but feel free to use any styling solution you. CSS-in-JS, CSS, etc.\
 * This component needs to be reusable and customizable. Multiple instances of this component
 * should be able to exist in the same view.
 *
 * The Slider should:
 * a. Allow for variable slide intervals, but default to 4 seconds
 * b. Should pause when a user is interacting with the component
 * c. The Slider should be able to take different types of slides. For example,
 * it could be a single image or a set of tiles. Reference Beatport.com for an example
 */

const Slider = ({
    scrollInterval = 4,
    slides,
    height = 50,
    width = 100,
    useGrid,
    controlsOnHover = false,
    nextControlIcon,
    prevControlIcon,
}) => {
    const [state, setState] = useState({
        currentSlideIdx: 0,
        isInteracting: false,
    });

    const {
        currentSlideIdx,
        isInteracting,
    } = state;
    const autoPlayRef = useRef();

    useEffect(() => {
        autoPlayRef.current = onNextSlideClick;
    });

    useEffect(() => {
        if (isInteracting || !scrollInterval) {
            return;
        }

        const play = () => {
            autoPlayRef.current();
        };
        const scrollIntervalMilliseconds = scrollInterval * 1000;
        const interval = setInterval(play, scrollIntervalMilliseconds);

        return () => clearInterval(interval);
    }, [state.isInteracting]);

    const onNextSlideClick = () => {
        let isLastSlide = currentSlideIdx === slides.length - 1;
        let nextSlideIdx = isLastSlide ? 0 : currentSlideIdx + 1;

        setState({
            ...state,
            currentSlideIdx: nextSlideIdx,
        });
    };

    const onPrevSlideClick = () => {
        let isFirstSlide = currentSlideIdx === 0;
        let prevSlideIdx = isFirstSlide ? slides.length - 1 : currentSlideIdx - 1;

        setState({
            ...state,
            currentSlideIdx: prevSlideIdx,
        });
    };

    let showIcons = controlsOnHover && isInteracting || !controlsOnHover;
    let isGrid = useGrid && Array.isArray(slides[currentSlideIdx]);

    return (
        <div
            css={SliderCSS({ height, width })}
            className="slider"
            onMouseEnter={() => setState({ ...state, isInteracting: true })}
            onMouseLeave={() => setState({ ...state, isInteracting: false })}
        >
            <SliderContent
                width={useCurrentWidth() || width}
                isGrid={useGrid}
            >
                {isGrid ?
                    slides[currentSlideIdx].map((slide, idx) => (
                        <Slide key={slide + idx} imageUrl={slide}/>
                    ))
                    :
                    <Slide imageUrl={slides[currentSlideIdx]}/>
                }
            </SliderContent>
            {showIcons &&
                <>
                    <SliderIcon
                        direction="left"
                        onClick={onPrevSlideClick}
                        icon={<Icon icon={prevControlIcon || 'arrow-left'}/>}
                    />
                    <SliderIcon
                        direction="right"
                        onClick={onNextSlideClick}
                        icon={<Icon icon={nextControlIcon || 'arrow-right'}/>}
                    />
                </>
            }
            <SliderPagination
                icon={<Icon icon="circle"/>}
                currentSlideIdx={currentSlideIdx}
                slides={slides}
            />
        </div>
    );
};

// STYLES
const SliderCSS = ({ height, width }) => (
    css`position: relative;
    height: ${height}vh;
    width: ${width}vw;
    margin: 0 auto;`
);

// PROPTYPES
Slider.propTypes = {
    slides: PropTypes.array,
    scrollInterval: PropTypes.number,
    height: PropTypes.number,
    width: PropTypes.number,
    useGrid: PropTypes.bool,
    controlsOnHover: PropTypes.bool,
    nextControlIcon: PropTypes.string,
    prevControlIcon: PropTypes.string,
};

// EXPORTS
export default Slider;
