// PACKAGES
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

const Icon = (props) => {
    const {
        icon,
        color,
        className,
        size,
        onClick,
    } = props;

    return (
        <FontAwesomeIcon
            icon={icon}
            color={color}
            className={className}
            size={size}
            onClick={onClick}
        />
    );
};

// EXPORTS
export default Icon;
