const initResize = () => {
    /**
     * Implement a function that handles JavaScript events. When the user clicks
     * and drags the `resize` node, its parent node `panel` should grow or shrink
     * vertically.
     */
    const resize = document.getElementById('resize');
    const panel = document.getElementById('panel');

    resize.addEventListener('mousedown', mouseDown);

    function mouseDown (e) {
        window.addEventListener('mousemove', mousemove);
        window.addEventListener('mouseup', mouseup);

        let prevY = e.clientY;

        function mousemove (e) {
            const rect = panel.getBoundingClientRect();

            panel.style.height = rect.height + (prevY - e.clientY);
            prevY = e.clientY;    
        }

        function mouseup () {
            window.removeEventListener("mousemove", mousemove);
            window.removeEventListener("mouseup", mouseup);
        };
    };
};

window.initResize = initResize;
export default initResize;
