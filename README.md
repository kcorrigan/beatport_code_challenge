# Beatport Code Challenges

All tests passing:

![Test results](https://i.imgur.com/Kvv1l5J.png)

## Running locally

Cloning the repo:

`git clone https://gitlab.com/kcorrigan/beatport_code_challenge.git`

run npm install:

`npm install`

to view the react app:

`npm run start`

## Prefix Flatten Object Snippet
Recursively navigate the object to populate a `flattenedObject`:

```
const prefixFlattenObject = (obj, prefix = '') => { 
    const flattenedObject = {};

    prefixFlattenObjectHelper({ obj, prefix, acc: flattenedObject });

    return flattenedObject;
};
  
const prefixFlattenObjectHelper = ({ obj, prefix, acc }) => {
    for (let [key, value] of Object.entries(obj)) {
        let updatedPrefix = prefix ? `${prefix}_${key}` : key;
  
        if (typeof value === "object" && !Array.isArray(value)) {
            prefixFlattenObjectHelper({ obj: value, prefix: updatedPrefix, acc });
        } else {
            acc[updatedPrefix] = value;
        }
    };  
}
```

## Integration Functions Snippet

```
export const integrationFunctions = integrationConfig => { 
    const api = {};

    for (let [key, value] of Object.entries(integrationConfig)) {
        const { methods, url } = value;
        api[key] = {};

        methods.map(method => {
            api[key][method] = function(params, config) {
                const finalUrl = params ? updatedUrl({ params, url }) : url;
                
                fetch(finalUrl, method, config);
            };
        })
    }

    return api;
};

const updatedUrl = ({ params, url }) => {
    let updatedUrl = url;

    for (let [param, value] of Object.entries(params)) {
        updatedUrl = url.replace(`:${param}`, value);
    }

    return updatedUrl;
}
```

## Resize Snippet

resize in action:

![Imgur](https://i.imgur.com/KLk1mMc.gif)

```
const initResize = () => {
    const resize = document.getElementById('resize');
    const panel = document.getElementById('panel');

    resize.addEventListener('mousedown', mouseDown);

    function mouseDown (e) {
        window.addEventListener('mousemove', mousemove);
        window.addEventListener('mouseup', mouseup);

        let prevY = e.clientY;

        function mousemove (e) {
            const rect = panel.getBoundingClientRect();

            panel.style.height = rect.height + (prevY - e.clientY);
            prevY = e.clientY;    
        }

        function mouseup () {
            window.removeEventListener("mousemove", mousemove);
            window.removeEventListener("mouseup", mouseup);
        };
    };
};
```

## React Component (Slider)

Slider in action:

![Slider component](https://i.imgur.com/yS9xNiV.mp4)

Additional libraries used:

* [Emotion](https://github.com/emotion-js/emotion)
* [Font Awesome](https://github.com/FortAwesome/react-fontawesome)
* [Prop Types](https://github.com/facebook/prop-types)

### Features
Functionality documentation of the Slider:

#### Slides
Accepts an array of strings `['slide1', 'slide2']`, or an array of arrays of strings `[[slide1, slide2], [slide3, slide4]]` (`useGrid` must be `true` for this to be valid):

`const images=['image1', 'image2]`

**example:**

```
<Slider
    slides={images}
/>
```

OR:

To stack images vertically in an individual slide:

`const gridImages=[['image', 'image2'], ['image3', 'image4', 'image5']]`

```
<Slider
    slides={gridImages}
    useGrid
/>

```

#### ScrollInterval
Enables autoplay. Time in seconds between slide changes:

**example:**

```
<Slider
    slides={images}
    scrollInterval={5}
/>
```
*Note*: If a `scrollInterval` time of 0 is passed in autoplay will be disabled.

#### Height and Width
Sets height and width in `vh` and `vw` respectively.

**example:**

```
<Slider
    slides={images}
    height={60}
    width={40}
/>
```

#### ControlsOnHover
Prevents display of slider controls until user interacts with slider:

**example:**

```
<Slider
    slides={images}
    controlsOnHover={true}
/>
```

#### NextControlIcon and PrevControlIcon
Allows control over what font awesome icon will be displayed on control toggles:

**example:**

```
<Slider
    slides={images}
    nextControlIcon="chevron-right"
    prevControlIcon="chevron-left"
/>
```

*NOTE*: Additional work still needs to be done to automatically add string values to font awesome library, so values need to be manually imported in the `icons` library.

#### Next steps:

Building the slider was a blast, some ideas for additional features and improvements:

* scrolling animation
* additional animations, including fadeout (most likely using [react transition group](https://github.com/reactjs/react-transition-group))
* Ability to add captions to slides
* Ability to click on dot to navigate to specific slide
* more robust and thorough testing, troubleshoot font-awesome console erros in test
